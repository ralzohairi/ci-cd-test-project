# FROM ubuntu:20.04 
FROM node:latest

# Install Packages
## Needed packages
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git curl &&\
    apt-get --assume-yes install zip &&\
    npm install -g @angular/cli
## Node js
# RUN     curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
#     apt-get install -y nodejs
## Zip (for compression)
# Assume yes to automatically say yes for prompts
# RUN apt-get --assume-yes install zip

# Create directory and download code for the application in it
# RUN mkdir /e-care-web-app && \
#     cd /e-care-web-app && \
#     # git clone https://github.com/ralzohairi/test-project-for-jenkins.git
#     git clone https://gitlab.com/ralzohairi/ci-cd-test-project.git

# Install project packages
# RUN npm install -g @angular/cli 
# && \
    # cd /e-care-web-app/ci-cd-test-project && \
    # export NG_CLI_ANALYTICS=off && \
    # npm i
# To stop any prompting

# Build the Angular App
# RUN     cd /e-care-web-app/ci-cd-test-project && \
#     ng build --prod --aot --outputHashing=all

# Compress the build files
# RUN     cd /e-care-web-app/ci-cd-test-project && \
#     zip -r e-care-web-build.zip dist

# ---------------
# Hit the nexus API
# IP of Nexus in network
# RUN curl -u admin:123456 http://172.18.0.2:8081/service/metrics/ping

# # Base image - for the image to be created and giving the image
# #              a name label to be able to make it volatile
# FROM ubuntu:20.04 
# # FROM builder-container:latest
# LABEL name=e-care-web-builder-image


# RUN ls

# # Installing Required Packages (including GIT)
# RUN     apt-get update && \
#     apt-get upgrade -y && \
#     apt-get install -y git curl

# RUN     curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
#     apt-get install -y nodejs

# # Create directory and download code for the application in it
# RUN mkdir /e-care-web-app && \
#     cd /e-care-web-app && \
#     # git clone https://github.com/ralzohairi/test-project-for-jenkins.git
#     git clone https://gitlab.com/ralzohairi/ci-cd-test-project.git


# # Install project packages
# RUN npm install -g @angular/cli && \
#     cd /e-care-web-app/ci-cd-test-project && \
#     export NG_CLI_ANALYTICS=off && \
#     npm i
# # To stop any prompting

# # Build the Angular App
# RUN     cd /e-care-web-app/ci-cd-test-project && \
#     ng build --prod --aot --outputHashing=all

# ## Install the zip package for file compression
# # Assume yes to automatically say yes for prompts
# RUN apt-get --assume-yes install zip

# # Compress the build files
# RUN     cd /e-care-web-app/ci-cd-test-project && \
#     zip -r e-care-web-build.zip dist

# RUN ls
# RUN     cd /e-care-web-app/ci-cd-test-project && \
#     ls

# # # Upload to Nexus Repository
# # RUN cd /e-care-web-app/ci-cd-test-project && \
#     #     # curl -v -u admin:123456 --upload-file e-care-web-build.zip http://192.168.240.138:8081/repository/e-care-web-angular-builds/latest-build/
#     # curl -v -u admin:123456 --upload-file e-care-web-build.zip http://localhost:8081/repository/e-care-web-angular-builds/
# # RUN curl -u admin:123456 http://localhost:8081/service/metrics/ping
# # RUN curl -u admin:123456 http://94.97.1.134:8081/service/metrics/ping
# RUN curl -u admin:123456 http://127.0.0.1:8081/service/metrics/ping

# # curl -v -u admin:123456 --upload-file x.zip http://localhost:8081/repository/e-care-web-angular-builds/fkug.zip
# # RUN cd /e-care-web-app/ci-cd-test-project && \
# # RUN curl -v -u admin:123456 -X POST 'http://localhost:8081/repository/e-care-web-angular-builds/'
# #  -F raw.directory=/e-care-web-app/ci-cd-test-project
# #   -F raw.assetN=e-care-web-build.zip
# #    -F raw.assetN.filename=tempxx.zip

# # curl -v --user 'admin:admin123' --upload-file ./test.png http://localhost:8081/repository/documentation/test.png


# # TODO:

# # TODO: Network IP changes - match it -> (System pref/settings/advanced)


# # -------------------------------------------

# # Old Commands used in image creation

# # Installing Required Packages (including GIT)
# # RUN     apt-get update && \
# #     apt-get upgrade -y && \
# #     apt-get install -y git curl


# # # # # Installing NodeJS
# # RUN     curl -fsSL https://deb.nodesource.com/setup_14.x | bash - && \
# #     apt-get install -y nodejs

# # # # # Creating the educative directory and downloading the code for the application in it
# # RUN     mkdir /e-care-web-app && \
# #     cd /e-care-web-app && \
# #     git clone https://github.com/ralzohairi/test-project-for-jenkins.git

# # # # # Installing Angular cli and node modules in angular directory
# # RUN     npm install -g @angular/cli && \
# #     cd /e-care-web-app/test-project-for-jenkins && \
# #     npm i

# ## Install the zip package for file compression
# # Assume yes to automatically say yes for prompts
# # RUN apt-get --assume-yes install zip

# # TODO: add TS to name or iv4 or sth

# # RUN cd /e-care-web-app/test-project-for-jenkins && \
# #     curl -v -u admin:123456 --upload-file e-care-web-build.zip http://localhost:8081/repository/e-care-web-angular-builds/
# # # TODO: