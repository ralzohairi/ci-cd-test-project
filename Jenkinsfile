pipeline {
    agent any
    stages {
        stage('Build Image') {
            steps {
                // Builds image using dockerfile
                sh 'docker build -t angular_build_r .'
            }
        }
        stage('Build') {
            steps {
                // Create a container out of the build image
                // Also, add the container to the network to be able to access nexus container
                sh 'docker run --name ecare-container -t -d --network=ecare-ci-cd-network angular_build_r'
                // Note: -t -d: keep it alive after exit (detached)
                // --rm: removes exited containers with that name

                // Create directory to import code in
                sh 'docker exec ecare-container mkdir /e-care-web-app'
                // Pull Code from Git Repository
                sh 'docker exec ecare-container bash -c "cd /e-care-web-app && git clone https://gitlab.com/ralzohairi/ci-cd-test-project.git"'
                // Install dependencies
                // sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && export NG_CLI_ANALYTICS=off && npm i"'
                sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && npm i"'
                // Create build
                sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && ng build --configuration=production --aot --outputHashing=all"'
                // Compress the build files
                sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && zip -r e-care-web-build-${BUILD_TAG}.zip dist"'
            }
        }
        stage('Publish') {
            steps {
                // Display network containers
                sh 'docker network inspect ecare-ci-cd-network'
                // Ping the nexus container from inside the build container
                sh 'docker exec ecare-container curl -u admin:123456 http://nexus:8081/service/metrics/ping'
                // Upload compressed directory to nexus repository
                sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && curl -v -u admin:123456 --upload-file e-care-web-build-${BUILD_TAG}.zip http://nexus:8081/repository/e-care-web-angular-builds/"'
            }
        }
        stage('Build Clean-up') {
            steps {
                // Stop and remove the build container
                sh 'docker stop ecare-container'
                sh 'docker rm ecare-container'

            // If need, remove the image (improvement)
            // sh 'docker rmi image_name'
            }
        }
        stage('Deploy') {
            steps {
                // List html nginx directory
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && ls"'
                // Delete previous build files
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && rm -rf ./*"'
                // Download the build in the correct nginx location
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && curl -u admin:123456 -X GET http://nexus:8081/repository/e-care-web-angular-builds/e-care-web-build-${BUILD_TAG}.zip  -O"'
                // Note: -0 to save it with the same name of the last bit of the path
                // List html nginx directory
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && ls"'
                // Unzip the downloded build file
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && unzip e-care-web-build-${BUILD_TAG}.zip"'
                // Move the content of the dist directory to the HTML folder
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && mv -v dist/ci-cd-test-project/* ./ "'
                // sh 'docker exec docker-nginx mv -v /usr/share/nginx/html/dist/* /usr/share/nginx/html'
                // Remove the dist directory and zip folder
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && rm -r dist"'
                sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && rm -r e-care-web-build-${BUILD_TAG}.zip"'
                // Start nginx container
                // NOTE: Cannot stop and start nginx inside container
                sh 'docker restart docker-nginx'
            }
        }
        // stage('Create Build') {
        //     steps {
        //         sh 'docker run --name ecare-container -t -d --network=ecare-ci-cd-network angular_build_r'
        //         sh 'docker exec ecare-container curl -u admin:123456 http://nexus:8081/service/metrics/ping'

    //         // -t -d: keep it alive after exit
    //         // --rm: removes exited containers with that name
    //         // Keep it alive
    //         // Run code through exec to upload build (to ensure is in network)
    //         // Create dir
    //         sh 'docker exec ecare-container mkdir /e-care-web-app'
    //         sh 'docker exec ecare-container ls'
    //         // Pull Code
    //         sh 'docker exec ecare-container bash -c "cd /e-care-web-app && git clone https://gitlab.com/ralzohairi/ci-cd-test-project.git"'
    //         // Install dependencies
    //         sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && export NG_CLI_ANALYTICS=off && npm i"'
    //         // Create build
    //         sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && ng build --prod --aot --outputHashing=all"'
    //         // Compress the build files
    //         sh 'docker exec ecare-container bash -c "cd /e-care-web-app/ci-cd-test-project && zip -r e-care-web-build.zip dist"'
    //     }
    // }
    // stage('Publish') {
    //     steps {
    //         // sh 'docker run --name ecare-container -t -d --network=ecare-ci-cd-network angular_build_r'
    //         // -t -d: keep it alive after exit
    //         // --rm: removes exited containers with that name
    //         // Keep it alive
    //         // Run code through exec to upload build (to ensure is in network)
    //         // Remove container (and image)
    //         sh 'docker network inspect ecare-ci-cd-network'
    //         // Use container name for reference as they are all in the same network
    //         sh 'docker exec ecare-container curl -u admin:123456 http://nexus:8081/service/metrics/ping'
    //     // sh 'docker exec ecare-container curl -v -u admin:123456 --upload-file /e-care-web-app/ci-cd-test-project/e-care-web-build.zip http://nexus:8081/repository/e-care-web-angular-builds/'
    //     // ${BUILD_TAG}
    //     }
    // }
    }
}

/*
pipeline {
    agent any
    stages {
        stage('Build and Upload to Nexus Repository using Dockerfile') {
            agent {
                // Use the Docker file created in the repoistory
                dockerfile true
            }
            steps {
                echo 'Executing docker image commands in Dockerfile...'
            }
        }
        stage('Remove Docker Builder Image') {
            steps {
                sh 'docker image prune --filter label=name=e-care-web-builder-image --all --force'
            }
        }
        // stage('Build') {
        //     agent {
        //         docker { image 'builder-container:latest' }
        //     }
        //     steps {
        //         // sh 'node -v'
        //         // sh 'cd && mkdir ./e-care-web-app'
        //         // sh 'mkdir ./e-care-web-app'
        //         // sh 'ls'
        //         // sh 'cd ./e-care-web-app && ls'
        //         // sh 'cd .. && ls'
        //         // sh 'cd .. && ls'
        //         // sh 'ls'
        //         // sh 'ls'
        //         // sh 'git clone https://github.com/ralzohairi/test-project-for-jenkins.git'
        //         // sh 'cd test-project-for-jenkins'
        //         // sh 'npm i'
        //         // sh 'ng build --prod --aot --outputHashing=all'
        //         // sh 'zip -r e-care-web-build.zip dist'
        //         // sh 'ls'
        //     }
        // }
        stage('Deploy') {
            // agent {
            //     docker { image 'node:14-alpine' }
            // }
            steps {
                echo 'Deploying....'
                // Deleted after deploying

            // Backup on instance
            }
        }
    }
}
*/

// Junk
                // Remove old build files/directories except the newly downloaded build folder
                // sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && find . -type f ! -name \"dist*\" -delete"'
                // sh 'docker exec docker-nginx bash -c "cd /usr/share/nginx/html && find . -type d ! -name \"dist*\" -delete"'
